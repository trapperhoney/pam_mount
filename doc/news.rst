v2.19 (2022-07-06)
==================

Enhancements:

* Added support for LUKS detached headers


v2.18 (2021-01-04)
==================

Import patches from the Debian project.

Changes:

* The build now uses PCRE2 instead of PCRE1.
* Cure mount.crypt recursively executing itself with -t crypt

Enhancements:

* Allow luserconf to be placed outside home directory
* Documentation updates


v2.17 (2020-11-20)
==================

Changes:

* Programs are now unequivocally installed to ${sbindir}
  (usually /usr(/local)/sbin) and never /sbin anymore.


v2.16 (2016-09-23)
==================

Enhancements:

* support building with OpenSSL 1.1.0 and test libressl 2.4.2

Changes:

* the sgrp match is now implemented by asking for the user's group list instead
  of asking for the group's user list. (The latter can easily be orders of
  magnitude larger, which is why some LDAP servers may be configured not to
  return the list at all.)


v2.15 (2014-12-01)
==================

Changes:

* util-linux >= 2.20 is required at runtime
  (just mentioning it again; it was already needed for building)
* use the helper= option to get umount.crypt invoked on calling umount
* remove unsupported -p0 mount option
* fix a crash in ehd_log
